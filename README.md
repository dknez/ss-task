# ss-task

# Code Challenge

Follow a path of characters & collect letters:

- Start at the character `@`
- Follow the path
- Collect letters
- Stop when you reach the character `x`

## Assignment

Write a piece of code that takes a map of characters as an input and outputs the collected letters and the list of characters of the travelled path.

Input:

- a map (2-dimensional array) of characters in a data format of your choice (can even be hard-coded as a global constant)

Output:

- Collected letters
- Path as characters

## Usage

First, install all the dependencies:
`npm install`

Path walking can be started in one of the following ways:

1. Start walk for a specific map which is loaded from the `maps` directory:
   `npm run start <fileName>` e.g. `npm run start task01`
2. Start acceptance tests which will load all the files located in the `maps` directory and print the results and test coverage. All existing tests can be started with:
   `npm run test`
