import { END_CHAR, START_CHAR, START_END_AMOUNT } from '../common/constants';
import { Map, MapCharacter } from '../common/interfaces';

export const nullOrEmptyMap = (value: Map): boolean => {
  return value === null || value.trim().length === 0;
};

export const validateMap = (map: Map): void => {
  try {
    if (nullOrEmptyMap(map)) {
      throw new Error(`No paths have been defined in the loaded map.`);
    }

    validateStartAndStopChars(map, START_CHAR, START_END_AMOUNT);
    validateStartAndStopChars(map, END_CHAR, START_END_AMOUNT);
  } catch (error) {
    throw error;
  }
};

const validateStartAndStopChars = (map: Map, char: MapCharacter, freq: number): boolean => {
  if (map.split(char).length - 1 != freq) {
    throw new Error(`Incorrect amount of ${char} characters. There should be exactly ${freq} ${char} character.`);
  }

  return true;
};
