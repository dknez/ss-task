import { readFileContent } from '../fileReader';

describe('readFileContent', () => {
  test('When input is valid, file content is returned', () => {
    expect(readFileContent('task17')).toEqual('randomtext');
  });

  test('When input is not valid, file content is not returned', () => {
    try {
      readFileContent('i_do_not_exist');
    } catch (error: any) {
      expect(error.code).toEqual('ENOENT');
    }
  });
});
