import { nullOrEmptyMap, validateMap } from '../mapValidator';

describe('nullOrEmptyString', () => {
  test('When valid string is given, return value is false', () => {
    expect(nullOrEmptyMap('valid string')).toBe(false);
  });

  test('When empty string is given, return value is false', () => {
    expect(nullOrEmptyMap('')).toBe(true);
  });
});

describe('validateMap', () => {
  test('When empty map is given, error is thrown', () => {
    expect(() => validateMap('')).toThrow();
  });

  test('When valid map is given, no error is thrown', () => {
    expect(() => validateMap('@-A-+-B-x')).not.toThrow();
  });

  test('When map with two starts is given, error is thrown', () => {
    expect(() => validateMap('@-A-+@B-x')).toThrowError(
      'Incorrect amount of @ characters. There should be exactly 1 @ character.'
    );
  });

  test('When map with two ends is given, error is thrown', () => {
    expect(() => validateMap('@-Ax+-B-x')).toThrowError(
      'Incorrect amount of x characters. There should be exactly 1 x character.'
    );
  });
});
