import { HORIZONTAL_CHAR, VERTICAL_CHAR } from '../../common/constants';
import { Direction } from '../../common/enums';
import {
  canTurn,
  findElement,
  getElementPositionForValue,
  getNewPositionForDirection,
  getOppositeDirection,
  getValueForPosition,
} from '../mapUtils';

describe('findElement', () => {
  test('When valid map is given, return element with given character', () => {
    expect(findElement(['@-A-+-B-x'], '@')).toMatchObject({ character: '@', position: { column: 0, row: 0 } });
  });

  test('When invalid map is given, return undefined', () => {
    expect(findElement(['--A-+-B-x'], '@')).toBe(undefined);
  });
});

describe('getValueForPosition', () => {
  test('When valid map rows and position is given, return wanted element', () => {
    expect(getValueForPosition(['@-A-+-B-x'], { row: 0, column: 0 })).toBe('@');
  });

  test('When invalid map rows is given, return start element', () => {
    expect(getValueForPosition([''], { row: 0, column: 0 })).toBe(undefined);
  });

  test('When invalid map rows is given, return start element', () => {
    try {
      getValueForPosition(['@-A-+-B-x'], { row: 55, column: 55 });
    } catch (error: any) {
      expect(error.message).toBe("Cannot read properties of undefined (reading '55')");
    }
  });
});

describe('getElementPositionAndValue', () => {
  test('For valid map row and character, return correct position', () => {
    expect(getElementPositionForValue(['@-A-+-B-x'], 'A')).toMatchObject({ row: 0, column: 2 });
  });

  test('When invalid map rows are given, return undefined', () => {
    expect(getElementPositionForValue([], 'A')).toBe(undefined);
  });

  test('When invalid character is provided, return undefined', () => {
    expect(getElementPositionForValue(['@-A-+-B-x'], 'Č')).toBe(undefined);
  });
});

describe('getNewPositionForDirection', () => {
  test('When direction is Up, row value is decreased by one', () => {
    expect(getNewPositionForDirection({ row: 5, column: 5 }, Direction.Up)).toMatchObject({
      position: { row: 4, column: 5 },
      direction: 'Up',
    });
  });

  test('When direction is Down, row value is increased by one', () => {
    expect(getNewPositionForDirection({ row: 5, column: 5 }, Direction.Down)).toMatchObject({
      position: { row: 6, column: 5 },
      direction: 'Down',
    });
  });

  test('When direction is Left, column value is decreased by one', () => {
    expect(getNewPositionForDirection({ row: 5, column: 5 }, Direction.Left)).toMatchObject({
      position: { row: 5, column: 4 },
      direction: 'Left',
    });
  });

  test('When direction is Right, column value is increased by one', () => {
    expect(getNewPositionForDirection({ row: 5, column: 5 }, Direction.Right)).toMatchObject({
      position: { row: 5, column: 6 },
      direction: 'Right',
    });
  });
});

test('When invalid direction is given, return "faulty" position and direction', () => {
  expect(getNewPositionForDirection({ row: 5, column: 5 }, {} as Direction)).toMatchObject({
    position: { row: -1, column: -1 },
    direction: 'None',
  });
});

describe('getOppositeDirection', () => {
  test('When direction is Up, return Down', () => {
    expect(getOppositeDirection(Direction.Up)).toBe('Down');
  });

  test('When direction is Down, return Up', () => {
    expect(getOppositeDirection(Direction.Down)).toBe('Up');
  });

  test('When direction is Left, return Right', () => {
    expect(getOppositeDirection(Direction.Left)).toBe('Right');
  });

  test('When direction is Right, return Left', () => {
    expect(getOppositeDirection(Direction.Right)).toBe('Left');
  });

  test('When direction is not defined, return None', () => {
    expect(getOppositeDirection({} as Direction)).toBe('None');
  });
});

describe('canTurn', () => {
  test('When char is vertical, direction is left, return false', () => {
    expect(canTurn(VERTICAL_CHAR, Direction.Left)).toBe(false);
  });

  test('When char is vertical, direction is right, return false', () => {
    expect(canTurn(VERTICAL_CHAR, Direction.Right)).toBe(false);
  });

  test('When char is horizontal, direction is up, return false', () => {
    expect(canTurn(HORIZONTAL_CHAR, Direction.Up)).toBe(false);
  });

  test('When char is horizontal, direction is down, return false', () => {
    expect(canTurn(HORIZONTAL_CHAR, Direction.Down)).toBe(false);
  });

  test('When char is vertical, direction is up, return true', () => {
    expect(canTurn(VERTICAL_CHAR, Direction.Up)).toBe(true);
  });

  test('When char is vertical, direction is down, return true', () => {
    expect(canTurn(VERTICAL_CHAR, Direction.Down)).toBe(true);
  });

  test('When char is horizontal, direction is right, return true', () => {
    expect(canTurn(HORIZONTAL_CHAR, Direction.Right)).toBe(true);
  });

  test('When char is horizontal, direction is left, return true', () => {
    expect(canTurn(HORIZONTAL_CHAR, Direction.Left)).toBe(true);
  });

  test('When char is empty string, direction is right, return true', () => {
    expect(canTurn('', Direction.Right)).toBe(true);
  });

  test('When char is null, direction is left, return true', () => {
    expect(canTurn(null as unknown as string, Direction.Left)).toBe(true);
  });
});
