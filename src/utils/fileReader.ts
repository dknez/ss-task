import fs from 'fs';
import path from 'path';
import { MAPS_FOLDER_NAME } from '../common/constants';
import { Map } from '../common/interfaces';

export const buildFilePath = (fileName: string): string => {
  return path.join(__dirname, '..', '..', MAPS_FOLDER_NAME, fileName);
};

export const readFileContent = (fileName: string): Map => {
  return fs.readFileSync(buildFilePath(fileName), 'utf8');
};
