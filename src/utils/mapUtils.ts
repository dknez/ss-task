import { HORIZONTAL_CHAR, VERTICAL_CHAR } from '../common/constants';
import { Direction } from '../common/enums';
import { MapCharacter, MapElement, MapRows, Move, Position } from '../common/interfaces';

export const findElement = (mapRows: MapRows, mapCharacter: MapCharacter): MapElement | undefined => {
  const position = getElementPositionForValue(mapRows, mapCharacter);

  if (position != null) {
    return { position: position, character: mapCharacter };
  }
};

export const getValueForPosition = (mapRows: MapRows, position: Position): MapCharacter => {
  return mapRows[position.row][position.column];
};

export const getElementPositionForValue = (mapRows: MapRows, value: MapCharacter): Position | undefined => {
  for (let row = 0; row < mapRows.length; row++) {
    const column = mapRows[row].indexOf(value);

    if (column === -1) {
      continue;
    }

    return { row, column };
  }
};

export const getNewPositionForDirection = (position: Position, direction: Direction): Move => {
  switch (direction) {
    case Direction.Up:
      return {
        position: { row: position.row - 1, column: position.column },
        direction: direction,
      };
    case Direction.Right:
      return {
        position: { row: position.row, column: position.column + 1 },
        direction: direction,
      };
    case Direction.Down:
      return {
        position: { row: position.row + 1, column: position.column },
        direction: direction,
      };
    case Direction.Left:
      return {
        position: { row: position.row, column: position.column - 1 },
        direction: direction,
      };
    default:
      return {
        position: { row: -1, column: -1 },
        direction: Direction.None,
      };
  }
};

export const getOppositeDirection = (direction: Direction): Direction => {
  switch (direction) {
    case Direction.Right:
      return Direction.Left;
    case Direction.Up:
      return Direction.Down;
    case Direction.Left:
      return Direction.Right;
    case Direction.Down:
      return Direction.Up;
    default:
      return Direction.None;
  }
};

export const canTurn = (mapCharacter: MapCharacter, direction: Direction): boolean => {
  if (mapCharacter === VERTICAL_CHAR && (direction === Direction.Left || direction === Direction.Right)) {
    return false;
  }
  if (mapCharacter === HORIZONTAL_CHAR && (direction === Direction.Up || direction === Direction.Down)) {
    return false;
  }
  return true;
};
