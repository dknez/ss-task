import os from 'os';
import { Map, MapRows, WalkResult } from './common/interfaces';
import { startWalking } from './service/mapWalker';
import { readFileContent } from './utils/fileReader';
import { validateMap } from './utils/mapValidator';

export const pathFollower = (fileName: string): WalkResult | undefined => {
  try {
    if (fileName == null || fileName.length == 0) {
      throw new Error('File name not provided');
    }

    const map: Map = readFileContent(fileName);
    console.log(`map: ${fileName}`);
    validateMap(map);
    const mapRows: MapRows = map.split(os.EOL);
    const result = startWalking(mapRows);

    console.log(`Letters ${result.collectedLetters}`);
    console.log(`Path as characters ${result.pathAsCharacters}\n`);

    return result;
  } catch (error) {
    console.log(`${error}\n`);
    return { collectedLetters: '', pathAsCharacters: '' };
  }
};

pathFollower(process.argv.slice(2)[0]);
