import { pathFollower } from '../main';

describe('acceptance tests', () => {
  test('task01', () => {
    const result = pathFollower('task01');
    expect(result).toMatchObject({ collectedLetters: 'ACB', pathAsCharacters: '@---A---+|C|+---+|+-B-x' });
  });

  test('task02', () => {
    const result = pathFollower('task02');
    expect(result).toMatchObject({ collectedLetters: 'ABCD', pathAsCharacters: '@|A+---B--+|+--C-+|-||+---D--+|x' });
  });

  test('task03', () => {
    const result = pathFollower('task03');
    expect(result).toMatchObject({ collectedLetters: 'ACB', pathAsCharacters: '@---A---+|||C---+|+-B-x' });
  });

  test('task04', () => {
    const result = pathFollower('task04');
    expect(result).toMatchObject({
      collectedLetters: 'GOONIES',
      pathAsCharacters: '@-G-O-+|+-+|O||+-O-N-+|I|+-+|+-I-+|ES|x',
    });
  });

  test('task05', () => {
    const result = pathFollower('task05');
    expect(result).toMatchObject({ collectedLetters: 'BLAH', pathAsCharacters: '@B+++B|+-L-+A+++A-+Hx' });
  });

  test('task06', () => {
    const result = pathFollower('task06');
    expect(result).toMatchObject({ collectedLetters: 'AB', pathAsCharacters: '@-A--+|+-B--x' });
  });

  test('task07', () => {
    try {
      pathFollower('task07');
    } catch (error: any) {
      expect(error.message).toBe('Error: Incorrect amount of @ characters. There should be exactly 1 @ character.');
    }
  });

  test('task08', () => {
    try {
      pathFollower('task08');
    } catch (error: any) {
      expect(error.message).toBe('Error: Incorrect amount of x characters. There should be exactly 1 x character.');
    }
  });

  test('task09', () => {
    try {
      pathFollower('task09');
    } catch (error: any) {
      expect(error.message).toBe('Error: Incorrect amount of @ characters. There should be exactly 1 @ character.');
    }
  });

  test('task10', () => {
    try {
      pathFollower('task10');
    } catch (error: any) {
      expect(error.message).toBe('Error: Incorrect amount of @ characters. There should be exactly 1 @ character.');
    }
  });

  test('task11', () => {
    try {
      pathFollower('task11');
    } catch (error: any) {
      expect(error.message).toBe('Error: Incorrect amount of @ characters. There should be exactly 1 @ character.');
    }
  });

  test('task12', () => {
    try {
      pathFollower('task12');
    } catch (error: any) {
      expect(error.message).toBe('Error: Incorrect amount of x characters. There should be exactly 1 x character.');
    }
  });

  test('task13', () => {
    try {
      pathFollower('task13');
    } catch (error: any) {
      expect(error.message).toBe('Error: Could not traverse map');
    }
  });

  test('task14', () => {
    try {
      pathFollower('task14');
    } catch (error: any) {
      expect(error.message).toBe('Error: Incorrect amount of x characters. There should be exactly 1 x character.');
    }
  });

  test('task15', () => {
    const result = pathFollower('task15');
    expect(result).toMatchObject({ collectedLetters: 'AB', pathAsCharacters: '@-A-+-B-x' });
  });

  test('task16', () => {
    try {
      pathFollower('task16');
    } catch (error: any) {
      expect(error.message).toBe('Error: No paths have been defined in the loaded map.');
    }
  });

  test('task17', () => {
    try {
      pathFollower('task17');
    } catch (error: any) {
      expect(error.message).toBe('Error: Incorrect amount of @ characters. There should be exactly 1 @ character.');
    }
  });

  test('Empty string as file name', () => {
    const result = pathFollower('');
    expect(result).toMatchObject({ collectedLetters: '', pathAsCharacters: '' });
  });

  test('null as file name', () => {
    const result = pathFollower(null as unknown as string);
    expect(result).toMatchObject({ collectedLetters: '', pathAsCharacters: '' });
  });
});
