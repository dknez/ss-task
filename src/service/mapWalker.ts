import { ALL_DIRECTIONS, END_CHAR, ONLY_LETTERS, START_CHAR, TURN_CHAR, VALID_CHARS } from '../common/constants';
import { Direction } from '../common/enums';
import { MapCharacter, MapElement, MapRows, Move, Position, WalkResult } from '../common/interfaces';
import {
  canTurn,
  findElement,
  getNewPositionForDirection,
  getOppositeDirection,
  getValueForPosition,
} from '../utils/mapUtils';

export const startWalking = (mapRows: MapRows): WalkResult => {
  const firstElement = findElement(mapRows, START_CHAR);
  let visitedPositions: Position[] = [];
  const currentDirection = Direction.None;

  if (firstElement == null) {
    throw new Error('Starting point is not defined.');
  }

  const res = walkMap(mapRows, firstElement, visitedPositions, currentDirection, START_CHAR, '');

  if (res === undefined) {
    throw new Error(`Could not traverse map`);
  }

  return { pathAsCharacters: res.pathAsCharacters, collectedLetters: res.collectedLetters };
};

export const walkMap = (
  mapRows: MapRows,
  currentElement: MapElement,
  visitedPositions: Position[],
  direction: Direction,
  visitedPath: string,
  collectedLetters: string
): WalkResult | undefined => {
  const nextMove = findNextMove(mapRows, currentElement, direction);

  visitedPositions.push(currentElement.position);

  if (nextMove) {
    const newChar: MapCharacter = getValueForPosition(mapRows, nextMove.position);
    currentElement = { position: nextMove.position, character: newChar };

    if (currentElement.character === END_CHAR) {
      visitedPath = addToPath(visitedPath, currentElement.character);
      return { pathAsCharacters: visitedPath, collectedLetters };
    }

    visitedPath = addToPath(visitedPath, currentElement.character);
    if (
      currentElement.character.match(ONLY_LETTERS) &&
      !visitedPositions.some(
        (pos) => pos.column === currentElement.position.column && pos.row === currentElement.position.row
      )
    ) {
      collectedLetters += currentElement.character;
      visitedPositions.push(currentElement.position);
    }

    return walkMap(mapRows, currentElement, visitedPositions, nextMove.direction, visitedPath, collectedLetters);
  }
};

export const findNextMove = (mapRows: MapRows, currentElement: MapElement, direction: Direction): Move | undefined => {
  const possibleMoves: Move[] = findPossibleMoves(currentElement, direction);

  for (const move of possibleMoves) {
    try {
      const nextChar: MapCharacter = getValueForPosition(mapRows, move.position);
      const currentChar: MapCharacter = currentElement.character;

      if (currentChar === TURN_CHAR && !canTurn(nextChar, move.direction)) {
        continue;
      }

      if (nextChar.match(ONLY_LETTERS) || VALID_CHARS.includes(nextChar)) {
        return move;
      }
    } catch (error) {
      continue;
    }
  }
  return;
};

export const findPossibleMoves = (currentElement: MapElement, direction: Direction): Move[] => {
  const possibleMoves: Move[] = [];

  const oppositeDirection: Direction = getOppositeDirection(direction);
  const possibleDirections: Direction[] = ALL_DIRECTIONS.filter(
    (dir) => dir !== direction && dir !== oppositeDirection
  );

  if (currentElement.position && direction !== Direction.None) {
    possibleMoves.push(getNewPositionForDirection(currentElement.position, direction));
  }

  possibleMoves.push(...possibleDirections.map((dir) => getNewPositionForDirection(currentElement.position, dir)));

  return possibleMoves;
};

const addToPath = (visitedPath: MapCharacter, currentChar: MapCharacter): MapCharacter => {
  visitedPath += currentChar;
  return visitedPath;
};
