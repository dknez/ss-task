import { HORIZONTAL_CHAR, START_CHAR, TURN_CHAR, VERTICAL_CHAR } from '../../common/constants';
import { Direction } from '../../common/enums';
import { MapElement, MapRows } from '../../common/interfaces';
import { findNextMove, findPossibleMoves, startWalking, walkMap } from '../mapWalker';

describe('findPossibleMoves', () => {
  test('For combination of current element and right direction, return three valid moves', () => {
    expect(findPossibleMoves({ position: { row: 0, column: 0 }, character: '-' }, Direction.Right)).toMatchObject([
      { direction: Direction.Right, position: { column: 1, row: 0 } },
      { direction: Direction.Up, position: { column: 0, row: -1 } },
      { direction: Direction.Down, position: { column: 0, row: 1 } },
      { direction: Direction.None, position: { column: -1, row: -1 } },
    ]);
  });

  test('For combination of current element and left direction, return three valid moves', () => {
    expect(findPossibleMoves({ position: { row: 0, column: 0 }, character: '-' }, Direction.Left)).toMatchObject([
      { direction: Direction.Left, position: { column: -1, row: 0 } },
      { direction: Direction.Up, position: { column: 0, row: -1 } },
      { direction: Direction.Down, position: { column: 0, row: 1 } },
      { direction: Direction.None, position: { column: -1, row: -1 } },
    ]);
  });

  test("For combination of current element and direction 'None', return four valid moves", () => {
    expect(findPossibleMoves({ position: { row: 0, column: 0 }, character: '-' }, Direction.None)).toMatchObject([
      { direction: Direction.Up, position: { column: 0, row: -1 } },
      { direction: Direction.Down, position: { column: 0, row: 1 } },
      { direction: Direction.Left, position: { column: -1, row: 0 } },
      { direction: Direction.Right, position: { column: 1, row: 0 } },
    ]);
  });

  test("For combination of current element and direction 'null', return four valid moves and one 'none'", () => {
    expect(
      findPossibleMoves({ position: { row: 0, column: 0 }, character: '-' }, null as unknown as Direction)
    ).toMatchObject([
      { direction: Direction.None, position: { column: -1, row: -1 } },
      { direction: Direction.Up, position: { column: 0, row: -1 } },
      { direction: Direction.Down, position: { column: 0, row: 1 } },
      { direction: Direction.Left, position: { column: -1, row: 0 } },
      { direction: Direction.Right, position: { column: 1, row: 0 } },
    ]);
  });

  test("For current element and direction set to 'null', error is thrown", () => {
    try {
      findPossibleMoves(null as unknown as MapElement, null as unknown as Direction);
    } catch (error: any) {
      expect(error.message).toBe("Cannot read properties of null (reading 'position')");
    }
  });
});

describe('findNextMove', () => {
  test('For provided map row, element and direction set to right, next move should be one column to the right', () => {
    expect(
      findNextMove(['--A-+-B-x'], { position: { row: 0, column: 0 }, character: START_CHAR }, Direction.Right)
    ).toMatchObject({ position: { column: 1, row: 0 }, direction: Direction.Right });
  });

  test('For provided map row, element and direction set to left, next move is undefined', () => {
    expect(
      findNextMove(['@--A-+-B-x'], { position: { row: 0, column: 0 }, character: START_CHAR }, Direction.Left)
    ).toBe(undefined);
  });

  test('For provided map row, element and direction set to up, next move should be one column to the left', () => {
    expect(
      findNextMove(['@--A-+-B-x'], { position: { row: 0, column: 5 }, character: TURN_CHAR }, Direction.Up)
    ).toMatchObject({ direction: Direction.Left, position: { column: 4, row: 0 } });
  });

  test('For provided map row, element and direction set to left, next move should be one column to the left', () => {
    expect(
      findNextMove(['@--A-+-B-x'], { position: { row: 0, column: 5 }, character: TURN_CHAR }, Direction.Left)
    ).toMatchObject({ direction: Direction.Left, position: { column: 4, row: 0 } });
  });

  test('For provided map row, element and direction set to right, next move should be one column to the right', () => {
    expect(
      findNextMove(['@--A-+-B-x'], { position: { row: 0, column: 5 }, character: TURN_CHAR }, Direction.Right)
    ).toMatchObject({ direction: Direction.Right, position: { column: 6, row: 0 } });
  });

  test('For provided map row, element and direction set to null, next move should be one column to the right', () => {
    expect(
      findNextMove(
        ['@--A-+-B-x'],
        { position: { row: 0, column: 5 }, character: VERTICAL_CHAR },
        null as unknown as Direction
      )
    ).toMatchObject({ direction: Direction.Left, position: { column: 4, row: 0 } });
  });

  test('For provided map row, element and direction set to null, next move should be one column to the right', () => {
    expect(
      findNextMove(
        ['@--A|+-B-x'],
        { position: { row: 0, column: 5 }, character: TURN_CHAR },
        null as unknown as Direction
      )
    ).toMatchObject({ direction: Direction.Right, position: { column: 6, row: 0 } });
  });

  test('For provided map row, element set to null and direction set to right, error is thrown', () => {
    try {
      findNextMove(['@--A-+-B-x'], null as unknown as MapElement, Direction.Right);
    } catch (error: any) {
      expect(error.message).toBe("Cannot read properties of null (reading 'position')");
    }
  });

  test('For map row set to null, defined element and direction set to right, next move should be undefined', () => {
    expect(
      findNextMove(
        null as unknown as MapRows,
        { position: { row: 0, column: 5 }, character: TURN_CHAR },
        Direction.Right
      )
    ).toBe(undefined);
  });
});

describe('walkMap', () => {
  test('For various combinations of input objects, correct values are returned', () => {
    expect(
      walkMap(['@--A-+-B-x'], { position: { row: 0, column: 0 }, character: START_CHAR }, [], Direction.Right, '', '')
    ).toMatchObject({ collectedLetters: 'AB', pathAsCharacters: '--A-+-B-x' });
  });

  test('For various combinations of input objects, correct values are returned', () => {
    expect(
      walkMap(['@--A-+-B-x'], { position: { row: 0, column: 0 }, character: START_CHAR }, [], Direction.None, '', '')
    ).toMatchObject({ collectedLetters: 'AB', pathAsCharacters: '--A-+-B-x' });
  });

  test('For various combinations of input objects, correct values are returned', () => {
    expect(
      walkMap(['@--A-+-B-x'], { position: { row: 0, column: 5 }, character: TURN_CHAR }, [], Direction.Down, '', '')
    ).toBe(undefined);
  });

  test('For various combinations of input objects, correct values are returned', () => {
    expect(
      walkMap(['@--A-+-B-x'], { position: { row: 0, column: 5 }, character: TURN_CHAR }, [], Direction.Down, '@-', '')
    ).toBe(undefined);
  });

  test('For various combinations of input objects, correct values are returned', () => {
    expect(
      walkMap(
        ['@--A-+-B-x'],
        { position: { row: 0, column: 2 }, character: HORIZONTAL_CHAR },
        [{ row: 0, column: 3 }],
        Direction.Right,
        '',
        ''
      )
    ).toMatchObject({ collectedLetters: 'B', pathAsCharacters: 'A-+-B-x' });
  });

  test('For various combinations of input objects, correct values are returned', () => {
    expect(
      walkMap(
        ['@--A-+-B-x'],
        { position: { row: 0, column: 2 }, character: HORIZONTAL_CHAR },
        [
          { row: 0, column: 3 },
          { row: 0, column: 1 },
        ],
        Direction.Left,
        '',
        ''
      )
    ).toBe(undefined);
  });

  test('For various combinations of input objects, correct values are returned', () => {
    expect(
      walkMap(
        ['@--A-+-B-x'],
        { position: { row: 0, column: 2 }, character: HORIZONTAL_CHAR },
        [
          { row: 0, column: 3 },
          { row: 0, column: 1 },
        ],
        null as unknown as Direction,
        '',
        ''
      )
    ).toBe(undefined);
  });
});

describe('startWalking', () => {
  test('For valid map, return correct response', () => {
    expect(startWalking(['@--A-+-B-x'])).toMatchObject({ collectedLetters: 'AB', pathAsCharacters: '@--A-+-B-x' });
  });

  test('For map without starting point, the error is received', () => {
    try {
      startWalking(['-A-+-B-x']);
    } catch (error: any) {
      expect(error.message).toBe('Starting point is not defined.');
    }
  });

  test('For map without ending point, the error is received', () => {
    try {
      startWalking(['@--A-+-B--']);
    } catch (error: any) {
      expect(error.message).toBe('Could not traverse map');
    }
  });
});
