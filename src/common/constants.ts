import { Direction } from './enums';

export const MAPS_FOLDER_NAME = 'maps';

export const START_END_AMOUNT = 1;
export const START_CHAR = '@';
export const END_CHAR = 'x';
export const HORIZONTAL_CHAR = '-';
export const VERTICAL_CHAR = '|';
export const TURN_CHAR = '+';
export const ONLY_LETTERS = /[A-Z]+/g;
export const VALID_CHARS = [
  START_CHAR,
  END_CHAR,
  HORIZONTAL_CHAR,
  VERTICAL_CHAR,
  TURN_CHAR,
];
export const ALL_DIRECTIONS = [
  Direction.Up,
  Direction.Down,
  Direction.Left,
  Direction.Right,
  Direction.None,
];
