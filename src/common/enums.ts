export enum Direction {
  Up = 'Up',
  Down = 'Down',
  Right = 'Right',
  Left = 'Left',
  None = 'None',
}
