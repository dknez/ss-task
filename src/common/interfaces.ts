import { Direction } from './enums';

export type Map = string;

export type MapCharacter = string;

export type MapRows = MapCharacter[];

export type RowTraveled = boolean[][];

export interface WalkResult {
  pathAsCharacters: string;
  collectedLetters: string;
}
export interface Position {
  row: number;
  column: number;
}

export interface MapElement {
  position: Position;
  character: MapCharacter;
}

export interface Move {
  position: Position;
  direction: Direction;
}
